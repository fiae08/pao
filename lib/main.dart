import 'package:flutter/material.dart';
import 'dart:convert';
import 'dart:html' as html;

final Map<String, dynamic> data = {};

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  static const title = 'PAO';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: title,
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
      ),
      home: const MyHomePage(title: title),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text(widget.title)),
        backgroundColor: const Color(0xff2f3e41),
      ),
      backgroundColor: const Color(0xff527a84),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: SelectionArea(
            child: SingleChildScrollView(
              child: Column(
                children: const [
                  TextBox(
                    title: '1. Projektbezeichnung (Auftrag/Teilauftrag)',
                    maxLength: 300,
                  ),
                  SizedBox(height: 8),
                  TextBox(
                    title: '1.1 Kurzform der Aufgabenstellung',
                    maxLength: 1000,
                  ),
                  SizedBox(height: 12),
                  Center(
                    child: TextHeader(
                      title: '2. Zielsetzung entwickeln / Soll-Konzept',
                    ),
                  ),
                  SizedBox(height: 4),
                  TextBox(
                    title: '2.1 Was soll am Ende des Projektes erreicht sein?',
                    maxLength: 1000,
                  ),
                  SizedBox(height: 8),
                  TextBox(
                    title: '2.2 Welche Anforderungen müssen erfüllt sein?',
                    maxLength: 1000,
                  ),
                  SizedBox(height: 8),
                  TextBox(
                    title:
                        '2.3 Welche Einschränkungen müssen berücksichtigt werden?',
                    maxLength: 1000,
                  ),
                  SizedBox(height: 12),
                  Center(
                    child: TextHeader(
                      title: '3. Projektstrukturplan entwickeln',
                    ),
                  ),
                  SizedBox(height: 4),
                  TextBox(
                    title:
                        '3.1 Was ist zur Erfüllung der Zielsetzung erforderlich?',
                    maxLength: 1000,
                  ),
                  SizedBox(height: 8),
                  TextBox(
                    title: '3.2 Hauptaufgaben auflisten',
                    maxLength: 1000,
                  ),
                  SizedBox(height: 8),
                  TextBox(
                    title: '3.3 Teilaufgaben auflisten',
                    maxLength: 1000,
                  ),
                  SizedBox(height: 8),
                  Center(
                    child: TextHeader(
                      title: '3. Projektstrukturplan entwickeln',
                    ),
                  ),
                  SizedBox(height: 12),
                  TextBox(
                    title: '4. Projektphasen mit Zeitplanung in Stunden',
                    maxLength: 1000,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: onPressed,
        tooltip: 'Download',
        child: const Icon(Icons.download),
      ),
    );
  }

  void onPressed() {
    final formState = _formKey.currentState;
    if (formState == null) {
      return;
    }
    formState.save();
    final text = const JsonEncoder.withIndent('  ').convert(data);
    // prepare
    final bytes = utf8.encode(text);
    final blob = html.Blob([bytes]);
    final url = html.Url.createObjectUrlFromBlob(blob);
    final anchor = html.document.createElement('a') as html.AnchorElement
      ..href = url
      ..style.display = 'none'
      ..download = 'pao.json';
    html.document.body?.children.add(anchor);
    // download
    anchor.click();
    // cleanup
    html.document.body?.children.remove(anchor);
    html.Url.revokeObjectUrl(url);
  }
}

class TextBox extends StatefulWidget {
  const TextBox({
    required this.title,
    required this.maxLength,
    Key? key,
  }) : super(key: key);

  final String title;
  final int maxLength;

  @override
  State<TextBox> createState() => _TextBoxState();
}

class _TextBoxState extends State<TextBox> {
  int textLength = 0;

  String get title => widget.title;

  int get maxLength => widget.maxLength;

  bool get isValid => textLength <= widget.maxLength;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        const SizedBox(height: 12),
        TextBar(
          isValid: isValid,
          title: title,
          maxLength: maxLength,
        ),
        const SizedBox(height: 4),
        TextInput(
          title: title,
          onChanged: onChanged,
          maxLength: maxLength,
          isValid: isValid,
        ),
        const SizedBox(height: 2),
        TextCount(
          isValid: isValid,
          textLength: textLength,
          maxLength: maxLength,
        ),
      ],
    );
  }

  void onChanged(String text) {
    setState(() => textLength = text.length);
  }
}

class TextInput extends StatelessWidget {
  const TextInput({
    Key? key,
    required this.title,
    required this.isValid,
    required this.maxLength,
    required this.onChanged,
  }) : super(key: key);

  final String title;
  final ValueChanged<String> onChanged;
  final int maxLength;
  final bool isValid;

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints(minHeight: maxLength / 3),
      child: IntrinsicHeight(
        child: TextFormField(
          onChanged: onChanged,
          onSaved: (value) => data[title] = value,
          expands: true,
          maxLines: null,
          decoration: InputDecoration(
            filled: true,
            fillColor:
                isValid ? const Color(0xffe1e1db) : const Color(0xfff2cae2),
          ),
          style: TextStyle(
            color: isValid ? Colors.black : Colors.red.shade900,
            fontStyle: isValid ? FontStyle.normal : FontStyle.italic,
          ),
        ),
      ),
    );
  }
}

class TextCount extends StatelessWidget {
  const TextCount({
    Key? key,
    required this.isValid,
    required int textLength,
    required this.maxLength,
  })  : _textLength = textLength,
        super(key: key);

  final bool isValid;
  final int _textLength;
  final int maxLength;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        color: isValid ? const Color(0xff6e9aa4) : Colors.amber,
        child: Padding(
          padding: const EdgeInsets.all(4.0),
          child: Text(
            '$_textLength / $maxLength',
            style: TextStyle(
              color: isValid ? Colors.black : Colors.red.shade900,
              fontSize: 20,
              fontWeight: FontWeight.w900,
            ),
          ),
        ),
      ),
    );
  }
}

class TextBar extends StatelessWidget {
  const TextBar({
    Key? key,
    required this.isValid,
    required this.title,
    required this.maxLength,
  }) : super(key: key);

  final bool isValid;
  final String title;
  final int maxLength;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8),
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: isValid ? const Color(0xff3f5d64) : Colors.red.shade800,
        border: Border.all(
          color: isValid ? const Color(0xff37484b) : Colors.black,
          width: 3.0,
        ),
        borderRadius: const BorderRadius.all(
          Radius.circular(10.0),
        ),
        boxShadow: const [
          BoxShadow(
            blurRadius: 2,
            color: Color(0xff2f3e41),
            offset: Offset(2, 2),
          ),
        ], // Make rounded corner of border
      ),
      child: Text(
        '$title (max. $maxLength Zeichen)',
        style: TextStyle(
          color: isValid ? Colors.grey.shade200 : Colors.amber,
          fontSize: 20,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}

class TextHeader extends StatelessWidget {
  const TextHeader({
    required this.title,
    Key? key,
  }) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8),
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: const Color(0xff3f5d64),
        border: Border.all(
          color: const Color(0xff37484b),
          width: 3.0,
        ),
        borderRadius: const BorderRadius.all(
          Radius.circular(10.0),
        ),
        boxShadow: const [
          BoxShadow(
            blurRadius: 2,
            color: Color(0xff2f3e41),
            offset: Offset(2, 2),
          ),
        ], // Make rounded corner of border
      ),
      child: Text(
        title,
        style: TextStyle(
          color: Colors.grey.shade200,
          fontSize: 20,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}
